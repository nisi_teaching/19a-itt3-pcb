
# Additional resources

## Standards

[IPC standards](http://www.hytekaalborg.dk/da/ipc-standarder)  
[Perfag standards](http://www.perfag.dk/specifikationer/specifikationer/)  
[Harmonized european standards](https://ec.europa.eu/growth/single-market/european-standards/[harmonised-standards_en)  
[CCC standards](https://www.nist.gov/standardsgov/compliance-faqs-china-compulsory-certification-ccc-marking)  

## Legislation
[ROHS](https://www.rohsguide.com/)  

## Other

[Solder alloys](https://en.wikipedia.org/wiki/Solder_alloys)  
[PCB substrates](https://www.essentracomponents.com/en-gb/news/guides/your-pcb-substrate-a-guide-to-materials)  
[Reflow, wave and hand soldering](https://youtu.be/saOHrw4ezGw)  
[Selective soldering](https://youtu.be/p-VImd2yW5s)  
[Pick and place machine](https://youtu.be/S8qkaTsr2_o)  
[Odd shape pick and place machine](https://youtu.be/EUxBMUS45Kg)  
[PCB factory](https://youtu.be/ljOoGyCso8s)  
[PCB Soldering Factory](https://youtu.be/24ehoo6RX8w)  
[Thermal profiling](https://youtu.be/XqOGCXw5QCw)  
[Intermetallic layer](https://www.indium.com/blog/intermetallics-in-soldering.php)  
[Materials lifetime](https://www.indium.com/blog/materials-lifetimes-a-modest-proposal.php)  
[PCB stencil](https://www.pcbgogo.com/Blog/What_is_PCB_stencil.html)  
[Link collection](https://blogs.mentor.com/jimmartens/blog/2019/06/21/ipc-7351-and-other-pcb-design-standards/)
[IPC checklist](http://www.ipc.org/4.0_Knowledge/4.1_Standards/PCBA-Checklist.pdf)

## Books

[Handbook of Electronic Assembly](https://www.smta.org/store/book_detail.cfm?book_id=436)  
