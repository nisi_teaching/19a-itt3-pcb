---
Week: 46
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 46 - Design files

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

Layers, Routing, gerber and drill files

### Learning goals

The student can:

* Route a PCB
* Apply design constraints according to fabrication house
* Create Gerber and drill files
* Order a PCB from a manufacturer

The student has knowledge about:

* Routing best practices
* Design constraints
* Gerber and drill file formats

## Deliverables

* Board routed
* Gerber and drill files created
* BOM, schematic and component placement documentation created
* Design ordered from JLCPCB
* Gitlab log updated

## Schedule Tuesday 2019-11-12

* 08:15 Introduction to the day + Q&A
* 08:30 Status
    * Students tells about the status of their work according to exercises
* 09:00 Work on deliverables and exercises + help from nisi
* 15:30 End of day

## Schedule Wednesday 2019-11-13

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Setup constraints

1. Go to [JLCPCB capabilities](https://jlcpcb.com/capabilities/Capabilities) and inspect what their capabilities are
2. Setup constraints in the constraint manager according to JLCPCB's capabilities.

If you need it you can recap the basics of the constraint manager from the [Nordcad workshop material](http://www.nordcad.dk/download/loesninger/komigang/KomNemtIGangmedOrCAD.zip) the file **4_PCB_Design_using_OrCAD.pdf** has this information

### Exercise 1 - Route the board

Route your PCB. It is important that you take current into consideration when desiding on trace width.

Some best practices in these articles:

1. [Ten-best-practices-of-PCB-design](https://www.edn.com/electronics-blogs/all-aboard-/4429390/Ten-best-practices-of-PCB-design)  
2. [pcb-design-guide](https://www.pannam.com/blog/pcb-design-guide/)  

Other best practices:

* When routing ensure that you do not have trace angles steeper than 45 degrees, if you have steeper angles acid from the manufacturing proces can lead to removing to much copper i these areas.

* Use copper pour and connect it to ground. Use copper pour on both sides of your pcb. This also means that when routing you do not connect ground nets with wires, they will connect automatically when you create the ground connected copper pour.  
This is especially important if you do larger PCB's. THey can bend if you do not have a sensible copper balance on both sides of your PCB.

[Video 19](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)

### Exercise 2 - Create outline

Create an outline for your PCB. THe outline is the size and shape of your PCB.

[Video 16-17](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)

You can do this exercise before exercise 0 if you prefer.

### Exercise 3 - Silkscreen

Add or edit component designators to make sure that it is easy to assemble and use the board.  
Add text that shows the name and revision of the board to be able to distinguish them if you make more revisions in the future.  
Add extra text to inputs and outputs with the purpose of making it easier to attach it to the outside world.  
[Video 20](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)

### Exercise 4 - Clear DRC errors

Make sure that you have no DRC errors in your design.  
If you have, resolve them by either moving traces or components.

### Exercise 3 - Create Gerber and Drill files

Use the menu nordcad->Output and postprocessing->Run post process to create a zip file that contains both your Gerber and drill files.  
You will probably be prompted to place a drill legend above your layout during the proces.

If you do not have the Nordcad menu use [Video 21](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx)  

Info on the [Gerber file format](https://www.ucamco.com/en/gerber) (make sure to click the tabs) and [Drill file formats](https://en.wikipedia.org/wiki/PCB_NC_formats)

### Exercise 4 - Inspect gerbers and drill file

To make sure that you have all your layers and drill files it is important to inspect them in an external program like [gerbv](https://sourceforge.net/projects/gerbv/) (free) or [gerbview](https://www.gerbview.com/) (paid)  

[Video 22](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) gives examples on how to do this.

### Exercise 5 - Order from JLC PCB

1. Create an account at [JLCPCB](https://jlcpcb.com/) 
2. Upload the zip containing your gerbers and drill files.
3. Pick delivery by UPS upon checkout, if you dont you will have to wait 3+ weeks. With UPS you should expect it within a week or less.

It is possible to order different designs in one order. This will save on parcel cost but might lead to a TAX bill.  
The UPS cost is app. 20 Euro and TAX will be 15 - 25% of the product cost + TAX handling which is around 125 DKK.
The tax limit is app. 80 DKK (~10 Euro)  

One thing to know when ordering anything from china is that it is near impossible during the [chinese new year](https://chinesenewyear.net/), which takes place in January and February.  
The reason for this is that manufacturing workers visit familiy, usually a long way from where they work.

### Exercise 6 - Documentation

Create a bill of materials and pdf's for the schematic as well as component placement.

[Video 25-26](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) gives examples on how to do this.

## Comments
 
[PCB editor Nordcad student forum](https://www.nordcad.dk/dk/student-forum/begynderkursus-en/learn-about-pcb-editor.htm)
