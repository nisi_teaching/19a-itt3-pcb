---
Week: 48
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 49 - scientific poster, how to pitch

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Prepare a poster for the poster presentation
* Prepare a pitch for the poster presentation

### Learning goals

The student can:

* Produce a poster that presents the PCB process
* Prepare a 5 minute pitch

The student has knowledge about:

* Presentation techniques

## Deliverables

* Poster uploaded to gitlab project
* Pitch script uploaded to gitlab project

## Schedule Tuesday 2019-12-03

* 08:15 Introduction to the day
    * Status on pcb's
    * Exam overview
    * Questions
* 09:00 Scientific posters and pitching
* 09:30 Work on deliverables and exercises
* 15:30 End of day

## Schedule Wednesday 2019-12-04

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Prepare poster

1. Prepare a scientific poster that includes clear points about your PCB design process
1. Use Microsoft publisher or similar application to produce the poster - Format is A0 (841 x 1189 mm)
2. Use transparent background images
3. Include an image or 3D rendering of your PCB (can also be 2D of the different layers)
4. Include relevant references and company logos
5. Include your name, email address and UCL logo
6. Focus on a clean layout from which you can pitch
7. Print the poster in A0 format (use the large printer on 1st floor in B building)

Inspiration for scientif posters:

[makesigns](https://www.makesigns.com/tutorials/scientific-poster-parts.aspx)  
[Berkeley](https://hsp.berkeley.edu/sites/default/files/ScientificPosters.pdf)

Poster examples:  
[PCB elective poster](https://eal-itt.gitlab.io/19a-itt3-pcb/poster_example.pdf)  
[NISI special subject poster](https://eal-itt.gitlab.io/19a-itt3-pcb/Poster_NS_special_subject.pdf)

### Exercise 1 - Prepare pitch

1. Prepare a 5 minute pitch based on your poster
2. Use [pitcherific](https://pitcherific.com/v1/app/education) to help you prepare your pitch

The pitch should reflect the process you went through when designing your PCB, from initial circuit decition to the final assembled and tested PCB.  
Use your gitlab log from each week of the course.

If you are in doubt about the content of your pitch, use the learning goals and practical goals from weekly plans as guidelines. 

If you can, include a brief demo of your PCB as part of the pitch.

### Exercise 2 - Practice presentation

When your poster and pitch is ready you should pratice your pitch.

I reccomend that you either present to somebody from class or maybe film yourself while pitching.

Remember to stay within 5 minutes.
 

## Comments
 
Henning has arranged a visit to EMC Jens, unfortunately I am not able to participate. 