---
Week: 43
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 43 - Introduction and Nordcad workshop

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Participate in Nordcad workshop
* Setup libraries in Orcad capture

### Learning goals
* The student will have an overview of the Orcad suite
* The student can create an Orcad project
* The student can create a schematic in Orcad capture

## Deliverables
* Gitlab project created with meaningful readme and folder structure
* Circuit chosen and documented with a schematic draft
* Components chosen and datasheets documented on gitlab


## Schedule Tuesday 2019-10-22

* 08:15 Introduction to the course
* 09:00 Nordcad workshop
* 15:30 End of day

## Schedule Wednesday 2019-10-23

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Setup gitlab project

Setup a gitlab project and use the naming **ITT3-PCB-YOURNAME**

### Exercise 1 - Circuit research

* Research and decide on the circuit you want to design and build
* Create a block diagram with logical subcircuits and specifications for input and output values between subcircuits.  
* Document with a schematic drawn in Orcad capture.
* If needed, identify and document test points and their values. 

### Exercise 2 - Component research

* Research which components you want to use
* Document your research in a document with reflections on the chosen components
* Gather datasheets for all chosen components and include them in the gitlab project

## Comments

[Introduction slides](https://docs.google.com/presentation/d/1YsukOngrXMKDU_IIcyL57_2fBpfOFJNsKgQTgA8SeXY/edit?usp=sharing)